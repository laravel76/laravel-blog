@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <h1 class="card-heading">{{$post->title}}</h1>
                @if($post->post_thumbnail !=='noimage.jpg' && $post->post_thumbnail !== '')
                <img class="card-img-top" src="/storage/uploads/{{$post->post_thumbnail}}" alt="Card image cap" style="width: 100%; height: 250px;">
                @else
                <img class="card-img-top" src="/storage/uploads/noimage.jpg" alt="Card image cap" style="width: 100%; height: 250px;">
                @endif
                <div class="card-body">
                    <p class="card-text">{{$post->body}}</p>
                    @if(!Auth::guest())
                        @if(Auth::user()->id == $post->user_id)
                        <div class="card-footer">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="{{$post->id}}/edit" class="btn btn-danger pull-left">Edit</a>
                                    </div>
                                    <div class="col-md-6">
                                        {!! Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'DELETE', 'class'=>'pull-right']) !!}
                                            {{Form::submit('Delete', ['class' => 'btn btn-danger' ])}}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        @endif 
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection