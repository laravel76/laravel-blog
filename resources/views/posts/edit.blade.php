@extends('layouts.app')

@section('content')
<div class="col-md-6">
        <a href="/posts" class="btn btn-success">cancel</a>
    {!! Form::open(['action' => ['PostsController@update', $post->id], 'method' => 'PUT', 'enctype' => 'multipart/form-data']) !!}
    
        <div class="form-group">
            {{Form::label('title')}}
            {{Form::text('title', $post->title, ['class' => 'form-control', 'placeholder' => 'Type post title'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Description')}}
            {{Form::textarea('body', $post->body, ['class' => 'form-control', 'placeholder', 'type post content'])}}
        </div>
        <div class="form-group">
            {{Form::file('post_thumbnail')}}
        </div>
        {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
    {!! Form::close() !!}
    </div>
@endsection