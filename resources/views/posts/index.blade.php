@extends('layouts.app')

@section('content')
<h2 class="card-heading text-center">{{$title}}</h2>
<div class="card mb-4">
    <div class="container">
        <div class="card-body">
            <div class="col-md-12">
                <div class="row">
                        @if (count($posts) > 0)
                            @foreach ($posts as $post)
                    <div class="col-md-6">
                        @if($post->post_thumbnail !=='noimage.jpg' && $post->post_thumbnail !=='')
                        <img class="card-img-top" src="/storage/uploads/{{$post->post_thumbnail}}" alt="Card image cap" style="width: 100%; height: 250px;">
                        @else
                        <img class="card-img-top" src="/storage/uploads/noimage.jpg" alt="Card image cap" style="width: 100%; height: 250px;">
                        @endif
                    </div>
                    <div class="col-md-6">
                        <a href="/posts/{{$post->id}}" class="permalink">
                            <h3 class="card-title">{{$post->title}}</h3>
                        </a>
                        <p class="card-text">{{$post->body}}</p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" text-muted">
                                    <small class="">Posted: {{$post->created_at}}</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <a href="posts/{{$post->id}}" class="btn btn-primary">Read More &rarr;</a>
                            </div>
                        </div>
                        <hr>
                    </div>
                    @endforeach   
                        {{-- {{$posts->links()}} --}}
                    @endif
                </div>
            </div>
        </div>    
    </div>
</div>
@endsection
