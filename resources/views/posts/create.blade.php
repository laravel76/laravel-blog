@extends('layouts.app')

@section('content')
<div class="col-md-6">
    <a href="/posts" class="btn btn-success">Back</a>
{!! Form::open(['action' => 'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

    <div class="form-group">
        {{Form::label('title', 'Title')}}
        {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Type post title'])}}
    </div>
    <div class="form-group">
        {{Form::label('body', 'Description')}}
        {{Form::textarea('body', '', ['class' => 'form-control', 'placeholder', 'type post content'])}}
    </div>
    <div class="form-group">
        {{Form::file('post_thumbnail')}}
    </div>
    {{Form::submit('Submit', ['class' => 'btn btn-primary'])}}
{!! Form::close() !!}
</div>
@endsection