@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    Your blog posts
                </div>
                <div class="card-body">
                    <ul class="lsit-group">
                        @if(count($posts)> 0)
                        @foreach($posts as $post)
                            <li class="list-group-item">{{$post->title}}</li>
                        @endforeach
                        @else
                        <li class="list-group-item">Hey! you don't have create any posts yet. please create a new post right now...</li>
                        @endif
                    </ul>
                </div>
                <div class="card-footer">
                    <a href="posts/create" class="btn btn-primary float-right">Create new post</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
